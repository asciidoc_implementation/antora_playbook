# help: @ Lists available make tasks
help:
	@egrep -oh '[0-9a-zA-Z_\.\-]+:.*?@ .*' $(MAKEFILE_LIST) | \
	awk 'BEGIN {FS = ":.*?@ "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | sort

# antora.build: @ Builds documentation production output (to build/site)
antora.build:
	docker-compose run -u $$(id -u) antora antora generate --clean antora-playbook.yml

# antora.run: @ Serves documentation output (on port 8051)
antora.run:
	docker-compose run --service-ports antora http-server build/site -c-1

# antora.watch: @ Watches for documentation changes and rebuilds (to build/site)
antora.watch:
	docker-compose run -u $$(id -u) -T antora onchange \
	-i antora-playbook.yml 'docs/**' \
	-- antora generate antora-playbook.yml

# antora.shell: @ Opens bash shell in antora container
antora.shell: CMD ?= /bin/sh
antora.shell:
	docker-compose run -u $$(id -u) antora $(CMD)
